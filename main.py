import discord
from discord.ext import commands
import os
import sys

if getattr(sys, 'frozen', False):
    import cogs

bot = commands.Bot(command_prefix="/")

extensions = ["cogs.manage_match","cogs.listAllRoleMembers","cogs.createPersonalChannel"]

if __name__ == "__main__":
    for extension in extensions:
        bot.load_extension(extension)


@bot.event
async def on_ready():
    await bot.change_presence(status=discord.Status.online, activity=discord.Game("プリンセスコネクト！Re:Dive"))

    print("Ready")

discord_token = os.environ["DISCORD_BOT_TOKEN"]

bot.run(discord_token, bot=True, reconnect=True)