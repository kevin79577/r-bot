# プリコネR-ダメコンBOT

プリンセスコネクト！Re:Dive (プリコネR)

<img src="https://gitlab.com/uploads/-/system/project/avatar/18920716/f795e8be8b87ef7fad014aefbf872e8e.png">

User needs to set environmental variable manually.
Include your discord bot token
```
set ENVIRONMENT=production
set DISCORD_BOT_TOKEN=***********************************************************
```

The following personal files need to be uploaded manually.
It is not included in this GitLab.
```
\secret\credentials.json
\configs\bosses.json
\configs\config.json
\configs\spreadsheet_config.json
```

Check Release for sample personal files

**last_session** empty folder needs to exist for **/finstop** command to work