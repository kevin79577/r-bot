# -*- coding: utf-8 -*-


from .auth import oauth, service_account
from .client import Client
from .models import Spreadsheet, Worksheet, Cell


def authorize(credentials, client_class=Client):

    client = client_class(auth=credentials)
    return client
