from discord.ext import commands
import discord
import os
from datetime import datetime
from pytz import timezone
import asyncio
import json

if os.environ["ENVIRONMENT"] == "production":
    import utils
else:
    from bot import utils


# TODO: Split into separate functions
# TODO: Create reservation class
# TODO: Do some popping of dictionaries at the right place
# TODO: Do some check like if they have checked-in
# TODO: Do the new numbering system
# TODO: Add new JSON


class ManageMatch(commands.Cog):
    def __init__(self, bot):
        self.bot = bot

        self.current_boss_msg = None
        self.boss_reservation_msgs = []

        self.user_reservation_msgs = {}
        self.user_reaction_name = {}

        self.round_num = 0
        self.boss_num = 0

        self.task_kill_msg = None

        self.config = utils.read_json("configs/config.json")
        self.bosses = utils.read_json("configs/bosses.json")

    @commands.command(name="start")
    async def start_command(self, ctx, round=None, boss=None):
        """
        starts a match
        :param ctx: context
        :param round: allows you to specify a round to start at
        :param boss: allows you to specify a boss to start at
        :return:
        """

        command_channel = discord.utils.get(ctx.guild.text_channels,
                                            id=self.config["COMMAND"]["CHANNEL_ID"])  # Finds command channel by ID
        # Finds reservation channel by ID
        reservation_channel = discord.utils.get(ctx.guild.text_channels, id=self.config["RESERVATION"]["CHANNEL_ID"])

        if int(ctx.channel.id) != int(command_channel.id):  # Checks if you executed the command in the command channel
            return

        # Checks if you saved the previous game with /finstop
        if os.path.exists("last_session/previous_game.json") is True:
            j = utils.read_json("last_session/previous_game.json")  # Reads previous match data

            self.boss_num = j["boss_num"]  # Sets previous boss number
            self.round_num = j["round_num"]  # Sets previous round number
            utils.spreadsheet_handler.questionnaire_col_count = j["questionnaire_col_count"]

            os.remove("last_session/previous_game.json")  # Removes previous save file
        elif round and boss is not None:
            self.round_num = int(round) - 1
            self.boss_num = int(boss) - 1
            utils.spreadsheet_handler.questionnaire_col_count = 2
            #utils.questionnaire_col_count = len(utils.spreadsheet_handler.questionnaire_worksheet.col_values(1))+1
        else:
            self.round_num = 0
            self.boss_num = 0
            utils.spreadsheet_handler.questionnaire_col_count = 2
            #utils.questionnaire_col_count = len(utils.spreadsheet_handler.questionnaire_worksheet.col_values(1))+1

        await command_channel.purge()  # Removes all messages from command channel
        await reservation_channel.purge()  # Removes all messages from reservation channel

        # Resets all the variables
        self.current_boss_msg = None
        self.boss_reservation_msgs = []
        self.user_reservation_msgs = {}
        self.user_reaction_name = {}

        # Resets all the spreadsheet variables
        utils.spreadsheet_handler.reservation_col_count = 2
        utils.spreadsheet_handler.reservation_spreadsheet_ids = 0
        utils.spreadsheet_handler.spreadsheet_user_id = {}
        
        if not self.config["RESERVATION"]["MODE"] == "OFF":
            utils.spreadsheet_handler.reservation_worksheet.resize(1, 7)  # Resizes the reservation spreadsheet
        utils.all_reservation_row_data = [["","","","","","",""]]

        # Sends boss information to command channel
        self.current_boss_msg = await utils.send_command_boss_msg(self.round_num, self.boss_num, command_channel)
        # Sends boss information to reservation channel
        self.boss_reservation_msgs = await utils.send_reservation_boss_msg(self.round_num, self.boss_num,
                                                                           reservation_channel)

    @commands.command(name="fin")
    async def fin_command(self, ctx):
        """
        this command tells the bot to move over to the next boss, it does all the appropriate tasks
        :param ctx: context
        :return:
        """
        # Gets the command channel
        command_channel = discord.utils.get(ctx.guild.text_channels, id=int(self.config["COMMAND"]["CHANNEL_ID"]))

        if ctx.channel != command_channel:  # Checks if the user executed the command in the correct channel
            return

        # Gets the role and channels
        mention_role = discord.utils.get(ctx.guild.roles, id=self.config["ROLE_ID"]["CLAN_ROLE_ID"])
        mention_channel = discord.utils.get(ctx.guild.text_channels, id=int(self.config["MENTION"]["CHANNEL_ID"]))

        # Sends message to user that the boss has been killed
        await mention_channel.send(f"{mention_role.mention} {self.config['MENTION']['MESSAGE']['NEXT']}")

        # Gets reservation channel
        reservation_channel = discord.utils.get(ctx.guild.text_channels,
                                                id=int(self.config["RESERVATION"]["CHANNEL_ID"]))

        # Checks if it is the last boss, if so prepares for the next round
        if self.boss_num == 4:
            self.boss_num = 0  # Starts from first boss
            self.round_num += 1  # Increments the round count

            self.boss_reservation_msgs = []  # Resets the array

            await reservation_channel.purge()  # Clears the channel

            # Sends the new reservation messages
            self.boss_reservation_msgs = await utils.send_reservation_boss_msg(self.round_num, self.boss_num,
                                                                               reservation_channel)
        else:
            self.boss_num += 1  # If not a new round increments boss count by 1

        await command_channel.purge()  # Clears command channel

        # Sends new boss message to command channel
        self.current_boss_msg = await utils.send_command_boss_msg(self.round_num, self.boss_num, command_channel)

        # Resets required variables
        self.user_reservation_msgs = {}
        self.user_reaction_name = {}

        # Resets the spreadsheet variables
        utils.spreadsheet_handler.spreadsheet_user_id = 0
        utils.spreadsheet_handler.spreadsheet_user_id = {}

        utils.spreadsheet_handler.reservation_col_count = 2

        #utils.spreadsheet_handler.reservation_worksheet.resize(1, 7)  # Resizes the spreadsheet, not needed would be rezized when data is batch updated
        oldReservationData = []
        for item in utils.all_reservation_row_data: #Workaround for making sure no old data is in the spreadsheet
            oldReservationData.append(["","","","","","",""])
        utils.all_reservation_row_data = [["","","","","","",""]]

        # Gets an updated version of the reservation message
        updated_boss_message = await reservation_channel.fetch_message(self.boss_reservation_msgs[self.boss_num].id)

        for reaction in updated_boss_message.reactions:  # Loops through all the reactions in message
            # Checks if the is one of the below
            if str(reaction.emoji) in [utils.physical_emoji, utils.magic_emoji, utils.carryover_emoji]:
                for user in await reaction.users().flatten():  # Loops through all the users
                    if user != self.bot.user:  # Checks if the loop through user isn't the bot
                        if reaction.message.id == self.boss_reservation_msgs[self.boss_num].id:
                            #utils.spreadsheet_handler.reservation_worksheet.resize(utils.spreadsheet_handler.reservation_col_count,7)  # Resizes the worksheet (removed because should not be needed)

                            reaction_name = {str(utils.physical_emoji): "物理", str(utils.magic_emoji): "魔法",
                                             str(utils.carryover_emoji): "持越"}.get(
                                str(reaction))  # Gets the Japanese name from the reaction emoji

                            # Insert row into reservation worksheet (Should be changed to append or dump all in the same request, also update cahche, or use cache for batch append)
                            dataToAppend = [f"{user.id}", "", reaction_name, "", "", "",utils.spreadsheet_handler.reservation_spreadsheet_ids]
                            #utils.spreadsheet_handler.reservation_worksheet.insert_row([f"{user.id}", None, reaction_name, None, None, None,utils.spreadsheet_handler.reservation_spreadsheet_ids],utils.spreadsheet_handler.reservation_col_count, value_input_option="USER_ENTERED")
                            utils.all_reservation_row_data.append(dataToAppend)
                            
                            # Adds it to a dictionary for later reference
                            utils.spreadsheet_handler.spreadsheet_user_id[f"{user.id}-{str(reaction.emoji)}"] = \
                                utils.spreadsheet_handler.reservation_spreadsheet_ids

                            # Increments counters
                            utils.spreadsheet_handler.reservation_col_count += 1
                            utils.spreadsheet_handler.reservation_spreadsheet_ids += 1

                            # Sets a key for the index of emojis
                            embed_field_index = [utils.physical_emoji, utils.magic_emoji, utils.carryover_emoji]

                            # Calculates the new value from the old value
                            new_value = int(
                                self.current_boss_msg.embeds[0].fields[
                                    embed_field_index.index(str(reaction.emoji))].value) + 1
                            # Gets the field name
                            field_name = self.current_boss_msg.embeds[0].fields[
                                embed_field_index.index(str(reaction.emoji))].name

                            # Edits the embed field
                            self.current_boss_msg.embeds[0].set_field_at(
                                index=embed_field_index.index(str(reaction.emoji)),
                                name=field_name, value=str(new_value), inline=True)

                            await self.current_boss_msg.edit(
                                embed=self.current_boss_msg.embeds[0])  # Publishes the edited field

                            # Gets the command channel
                            command_channel = discord.utils.get(user.guild.text_channels,
                                                                id=int(self.config["COMMAND"]["CHANNEL_ID"]))

                            confirm_message = await command_channel.send(
                                f"{str(reaction.emoji)} | **{user.display_name}**")  # Sends a confirmation message

                            # Adds reactions to message
                            await confirm_message.add_reaction(utils.confirm_emoji)
                            await confirm_message.add_reaction(utils.last_attack_emoji)

                            # Adds information to a dictionary to use later
                            self.user_reservation_msgs[confirm_message.id] = user
                            self.user_reaction_name[confirm_message.id] = reaction_name
                            
        #update whole spreadsheet here
        try:
            for item in utils.all_reservation_row_data:
                del oldReservationData[0]
        except:
            pass
        dataToSend = [{"range":"A1","values":utils.all_reservation_row_data+oldReservationData}] #workaround to make it also remove old boss data
        if not self.config["RESERVATION"]["MODE"] == "OFF":
            utils.spreadsheet_handler.reservation_worksheet.batch_update(dataToSend,value_input_option="USER_ENTERED")

    @commands.command(name="finstop")
    async def finstop_command(self, ctx):
        """
        a command to stop the whole match but go to the next round and save the game state to a file
        :param ctx: context
        :return:
        """
        # Gets all the channels needed
        command_channel = discord.utils.get(ctx.guild.text_channels, id=self.config["COMMAND"]["CHANNEL_ID"])

        if ctx.channel != command_channel:  # Checks if the user executed the command in the right channel
            return

        # Gets all the channels needed
        mention_channel = discord.utils.get(ctx.guild.text_channels, id=int(self.config["MENTION"]["CHANNEL_ID"]))
        reservation_channel = discord.utils.get(ctx.guild.text_channels, id=self.config["RESERVATION"]["CHANNEL_ID"])

        # Gets the role needed
        mention_role = discord.utils.get(ctx.guild.roles, id=self.config["ROLE_ID"]["CLAN_ROLE_ID"])

        # Sends a message to the user to tell them the match as ended
        await mention_channel.send(f"{mention_role.mention} {self.config['MENTION']['MESSAGE']['STOP']}")

        if self.boss_num == 4:  # Checks if it is boss before the next round
            # Increments round counter and resets boss counter to the first boss
            self.boss_num = 0
            self.round_num += 1
        else:
            self.boss_num += 1  # if not then increments the boss counter

        json_dict = {"boss_num": self.boss_num, "round_num": self.round_num,
                     "questionnaire_col_count": utils.spreadsheet_handler.questionnaire_col_count}  # Creates a dictionary to dump the file

        with open("last_session/previous_game.json", "w+") as file:  # Dumps the dict to the file for later reading
            json.dump(json_dict, file)

        # Clears all the needed channels
        await command_channel.purge()
        await reservation_channel.purge()

        # Resets all the variables
        self.current_boss_msg = None
        self.boss_reservation_msgs = []
        self.user_reservation_msgs = {}
        self.user_reaction_name = {}
        self.round_num = 0
        self.boss_num = 0

        # Resets all the spreadsheet variables
        utils.spreadsheet_handler.reservation_col_count = 2
        utils.spreadsheet_handler.questionnaire_col_count = 2
        #utils.questionnaire_col_count = len(utils.spreadsheet_handler.questionnaire_worksheet.col_values(1))+1
        utils.spreadsheet_handler.reservation_spreadsheet_ids = 0
        utils.spreadsheet_handler.spreadsheet_user_id = {}
        utils.all_reservation_row_data = []

        #utils.spreadsheet_handler.reservation_worksheet.resize(1, 7)  # Resizes spreadsheet (removed because not needed)

    @commands.command(name="r")
    async def damage_command(self, ctx, damage: str, description: str = None):
        """
        a command that allows the user to specify the damage they did to the bot
        :param ctx: context
        :param damage: amount of damage
        :param description: description
        :return:
        """
		
        # Loops through all the messages in the current reservations
        for id in self.user_reservation_msgs.keys():
            # Checks if current message author is the same as the one who executed the command
            if self.user_reservation_msgs.get(id).id == ctx.author.id:
                # Edits the message with damage
                command_channel = discord.utils.get(ctx.guild.text_channels, id=self.config["COMMAND"]["CHANNEL_ID"])
                message = await command_channel.fetch_message(id)
                
                if not "`" in message.content: #Need to check it has not already damage inputed
                    await message.edit(content=f"{message.content[:26]} `{damage}` {message.content[27::]}\n{description}")
                    break

        # Gets all the cells with user id in it (returns a list of cell objects), removed, will search local cache instead
        #all_cells = utils.spreadsheet_handler.reservation_worksheet.findall(f"{ctx.author.id}")
        
        #utils.all_reservation_row_data = utils.spreadsheet_handler.reservation_worksheet.get_all_values() #Remove this, just for testing
        rowCounter = 1
        for row in utils.all_reservation_row_data:
            if str(row[0]) == str(ctx.author.id):
                if row[4] == "":
                    # Adds the information about the attack to spreadsheet
                    #utils.spreadsheet_handler.reservation_worksheet.update_cell(cell.row, 5, damage)
                   # utils.spreadsheet_handler.reservation_worksheet.update_cell(cell.row, 6, description)
                   
                   #Inserting both in the same api call, and breaking after first find
                   cells = [utils.googleSheetAPI.models.Cell(rowCounter, 5, damage),utils.googleSheetAPI.models.Cell(rowCounter, 6, description)]
                   if not self.config["RESERVATION"]["MODE"] == "OFF":
                       utils.spreadsheet_handler.reservation_worksheet.update_cells(cells)
                   
                   #row counts starts at 0 but sheet start at 1
                   utils.all_reservation_row_data[rowCounter-1][4] = damage
                   utils.all_reservation_row_data[rowCounter-1][5] = description
                   break
            rowCounter = rowCounter +1

    @commands.command(name="tk")
    async def task_kill_command(self, ctx):
        """
        1. removes everyone with the task kill role
        2. clears the task kill channel
        3. sends new task kill message and adds reaction
        :param ctx: context
        :return:
        """
        
        command_channel = discord.utils.get(ctx.guild.text_channels, id=int(self.config["TASK_KILL"]["CHANNEL_ID"]))

        if ctx.channel != command_channel:  # Checks if the user executed the command in the right channel
            print(command_channel.id)
            return
        
        
        # Gets the task kill role and loops through and removes everyone with the role
        role = discord.utils.get(ctx.guild.roles, id=self.config["ROLE_ID"]["TASK_KILL_ROLE_ID"])
        for member in role.members:
            await member.remove_roles(role)

        # Gets the task kill channel and removes all messages
        channel = discord.utils.get(ctx.guild.text_channels, id=int(self.config["TASK_KILL"]["CHANNEL_ID"]))
        await channel.purge()
        
        
        #channel = discord.utils.get(ctx.guild.text_channels, id=int(self.config["TASK_KILL"]["CHANNEL_ID"]))
        date = datetime.now(timezone("Japan"))  # Gets the local time in JST (Japan)

        # Sends message and adds reaction
        message = await channel.send(f"{date.strftime('%m/%d')} {self.config['TASK_KILL']['UI_TEXT_TASK_KILL']}")
        await message.add_reaction(utils.task_kill_emoji)

        self.task_kill_msg = message  # Sets message as a global class variable

    @commands.Cog.listener()
    async def on_raw_reaction_add(self, payload):
        """
		Changed to raw reaction because cache clearing
        """
        guildObj = await self.bot.fetch_guild(int(payload.guild_id))
        user = payload.member
        channel = self.bot.get_channel(int(payload.channel_id))
        message = await channel.fetch_message(int(payload.message_id))
        
        if self.task_kill_msg is not None:  # Checks if the task kill message has been sent
            # Checks it the message
            if str(
                    payload.emoji) == utils.task_kill_emoji and user.id != self.bot.user.id and message.id == self.task_kill_msg.id:
                """
                1. runs checks
                2. adds the task kill role to user
                3. adds a new row to the questionnaire worksheet
                """
                
                role = discord.utils.get(user.guild.roles,
                                         id=self.config["ROLE_ID"]["TASK_KILL_ROLE_ID"])  # Gets task kill role by ID
                await user.add_roles(role)  # Adds role to user
                
                '''Not sure why this is needed anymore
                utils.spreadsheet_handler.questionnaire_worksheet.resize(
                    utils.spreadsheet_handler.questionnaire_col_count, 8)  # Resize questionnaire worksheet
                '''
                
                #currentData = utils.spreadsheet_handler.questionnaire_worksheet.get_all_values()
                #print(currentData)
                #utils.spreadsheet_handler.questionnaire_worksheet.update('A'+str(utils.spreadsheet_handler.questionnaire_col_count), to_update["values"])
                
                dataToAppend = [str(datetime.now(timezone("Japan")).strftime("%Y/%m/%d %H:%M:%S")), f"{user.id}", "", "", "","", "","TRUE"]
                if utils.spreadsheet_handler.questionnaire_col_count > 1:
                    #Inserts are 2 reqs
                    if not self.config["RESERVATION"]["MODE"] == "OFF":
                        utils.spreadsheet_handler.questionnaire_worksheet.insert_row(dataToAppend, 2, value_input_option="USER_ENTERED")  # Insert row with information about the user
                else:
                    if not self.config["RESERVATION"]["MODE"] == "OFF":
                        utils.spreadsheet_handler.questionnaire_worksheet.append_row(dataToAppend, value_input_option='USER_ENTERED', insert_data_option="INSERT_ROWS",table_range="A2")

                utils.spreadsheet_handler.questionnaire_col_count += 1  # Increment the column count by 1

        # Checks reaction
        if str(payload.emoji) in [utils.physical_emoji, utils.magic_emoji, utils.carryover_emoji] and user != \
                self.bot.user:
            """
            1. run checks
            2. adds the reservation information to the spreadsheet
            3. changes the value on the command channel's embed
            4. sends a confirmation message to the command channel
            5. adds reactions
            6. sets the confirm message information for later use
            """
                
            if message.id == self.boss_reservation_msgs[self.boss_num].id:
                #utils.spreadsheet_handler.reservation_worksheet.resize(utils.spreadsheet_handler.reservation_col_count,7)  # Resizes the worksheet (removed because the bot will append instead)

                reaction_name = {str(utils.physical_emoji): "物理", str(utils.magic_emoji): "魔法",
                                 str(utils.carryover_emoji): "持越"}.get(
                    str(payload.emoji))  # Gets the Japanese name from the reaction emoji

                # Insert row into reservation worksheet (changed to append)
                '''
                utils.spreadsheet_handler.reservation_worksheet.insert_row(
                    [f"{user.id}", None, reaction_name, None, None, None,
                     utils.spreadsheet_handler.reservation_spreadsheet_ids],
                    utils.spreadsheet_handler.reservation_col_count, value_input_option="USER_ENTERED")
                '''
                if not self.config["RESERVATION"]["MODE"] == "OFF":
                    utils.spreadsheet_handler.reservation_worksheet.append_row([f"{user.id}", None, reaction_name, None, None, None,utils.spreadsheet_handler.reservation_spreadsheet_ids], value_input_option='USER_ENTERED', insert_data_option="INSERT_ROWS",table_range="A2")
                utils.all_reservation_row_data.append([f"{user.id}", "", reaction_name, "", "", "",utils.spreadsheet_handler.reservation_spreadsheet_ids])
                
                utils.spreadsheet_handler.spreadsheet_user_id[f"{user.id}-{str(payload.emoji)}"] = \
                    utils.spreadsheet_handler.reservation_spreadsheet_ids

                utils.spreadsheet_handler.reservation_col_count += 1
                utils.spreadsheet_handler.reservation_spreadsheet_ids += 1

                # To help the function find the index of the embed
                embed_field_index = [utils.physical_emoji, utils.magic_emoji, utils.carryover_emoji]

                # Calculates the new value from the old value
                new_value = int(
                    self.current_boss_msg.embeds[0].fields[embed_field_index.index(str(payload.emoji))].value) + 1
                # Gets the field name
                field_name = self.current_boss_msg.embeds[0].fields[embed_field_index.index(str(payload.emoji))].name

                # Edits the embed field
                self.current_boss_msg.embeds[0].set_field_at(index=embed_field_index.index(str(payload.emoji)),
                                                             name=field_name, value=str(new_value), inline=True)

                await self.current_boss_msg.edit(embed=self.current_boss_msg.embeds[0])  # Publishes the edited field

                command_channel = discord.utils.get(user.guild.text_channels,
                                                    id=int(self.config["COMMAND"][
                                                               "CHANNEL_ID"]))  # Gets the command channel

                confirm_message = await command_channel.send(
                    f"{str(payload.emoji)} | **{user.display_name}**")  # Sends a confirmation message

                # Adds reactions to message
                await confirm_message.add_reaction(utils.confirm_emoji)
                await confirm_message.add_reaction(utils.last_attack_emoji)

                # Adds information to a dictionary to use later
                self.user_reservation_msgs[confirm_message.id] = user
                self.user_reaction_name[confirm_message.id] = reaction_name

        if str(payload.emoji) in [utils.confirm_emoji, utils.last_attack_emoji] and user.id != self.bot.user.id and \
                message.id in self.user_reservation_msgs.keys():
            # Gets the mention channel
            mention_channel = discord.utils.get(user.guild.text_channels, id=self.config["MENTION"]["CHANNEL_ID"])
            # Puts the user in a variable for ease of use
            reservation_user = self.user_reservation_msgs.get(message.id)
            # A key for the bot to get the index of the emojis
            embed_field_index = [utils.physical_emoji, utils.magic_emoji, utils.carryover_emoji,
                                 utils.check_in_emoji, utils.confirm_emoji, utils.last_attack_emoji]

            if str(payload.emoji) == utils.confirm_emoji:  # Checks if the reaction was a check mark
                # Sends the required message to mention channel
                await mention_channel.send(f"{reservation_user.mention} {self.config['MENTION']['MESSAGE']['CHECK']}")

                # Calculates the new value for the embed
                new_value = int(
                    self.current_boss_msg.embeds[0].fields[embed_field_index.index(utils.confirm_emoji)].value) + 1
                # Gets the field name
                field_name = self.current_boss_msg.embeds[0].fields[embed_field_index.index(utils.confirm_emoji)].name

                # Sets the new information
                self.current_boss_msg.embeds[0].set_field_at(index=embed_field_index.index(utils.confirm_emoji),
                                                             name=field_name, value=str(new_value), inline=True)

                # Makes the edit
                await self.current_boss_msg.edit(embed=self.current_boss_msg.embeds[0])
            elif str(payload.emoji) == utils.last_attack_emoji:
                # Sends the required message to mention channel
                await mention_channel.send(f"{reservation_user.mention} {self.config['MENTION']['MESSAGE']['CROSS']}")

                # Gets the value of the embed field
                value = self.current_boss_msg.embeds[0].fields[embed_field_index.index(utils.last_attack_emoji)].value

                if value == "-":  # Checks if the field has a -
                    new_value = f"{reservation_user.mention}"  # If so adds the user's name without a \n
                else:
                    new_value = value + f"\n{reservation_user.mention}"  # Else adds it with a \n

                # Gets the field name
                field_name = self.current_boss_msg.embeds[0].fields[
                    embed_field_index.index(utils.last_attack_emoji)].name

                # Adds the information to the field
                self.current_boss_msg.embeds[0].set_field_at(index=embed_field_index.index(utils.last_attack_emoji),
                                                             name=field_name, value=str(new_value), inline=True)

                # Makes the edit
                await self.current_boss_msg.edit(embed=self.current_boss_msg.embeds[0])
                
            if self.config["QUESTIONNAIRE"]["MODE"] == "OFF":
                return
            elif (self.config["QUESTIONNAIRE"]["MODE"] == "DM") or (self.config["QUESTIONNAIRE"]["MODE"] == "CHANNEL"):
                if (self.config["QUESTIONNAIRE"]["MODE"] == "DM"):
                    if reservation_user.dm_channel is None:  # Creates a DM channel if not is already made
                        await reservation_user.create_dm()
                else:
                    userChannel = utils.memberChannels[str(reservation_user.id)]
                    channel = self.bot.get_channel(int(userChannel))
                    if channel is None:
                        return

                # Asks for the users damage
                if (self.config["QUESTIONNAIRE"]["MODE"] == "DM"):
                    damage_message = await reservation_user.dm_channel.send(
                        self.config["QUESTIONNAIRE"]["QUESTION_TEXT"]["DAMAGE"])
                else:
                    damage_message = await channel.send(
                        self.config["QUESTIONNAIRE"]["QUESTION_TEXT"]["DAMAGE"])
                await damage_message.add_reaction(utils.discard_emoji)  # Adds a reaction to cancel the questionnaire

                # Specifies a check for the information being gathered
                def check(m):
                    if (self.config["QUESTIONNAIRE"]["MODE"] == "DM"):
                        return m.author == reservation_user and m.channel == reservation_user.dm_channel
                    else:
                        return m.channel == channel

                try:
                    damage = await self.bot.wait_for("message", check=check, timeout=60)  # Waits for a message

                    # Gets a updated message of the damage question
                    if (self.config["QUESTIONNAIRE"]["MODE"] == "DM"):
                        updated_message = await reservation_user.dm_channel.fetch_message(damage_message.id)
                    else:
                        updated_message = await channel.fetch_message(damage_message.id)

                    # Loops through all the reactions on the message
                    for r in updated_message.reactions:
                        for u in await r.users().flatten():  # And then through all the users
                            # If there is a cross emoji from the user then terminate
                            if str(r.emoji) == utils.discard_emoji and u.id != self.bot.user.id:
                                return
                except asyncio.TimeoutError:  # If they don't reply terminate
                    return

                if str(payload.emoji) == utils.last_attack_emoji:  # If the commander reacted with X
                    if (self.config["QUESTIONNAIRE"]["MODE"] == "DM"):
                        carryover_message = await reservation_user.send(self.config["QUESTIONNAIRE"]["QUESTION_TEXT"]["CARRYOVER_TIME"])  # Ask for users carryover time
                    else:
                        carryover_message = await channel.send(self.config["QUESTIONNAIRE"]["QUESTION_TEXT"]["CARRYOVER_TIME"])  # Ask for users carryover time
                    await carryover_message.add_reaction(utils.discard_emoji)
                    if (self.config["QUESTIONNAIRE"]["MODE"] == "DM"):
                        carryover_message = await reservation_user.dm_channel.fetch_message(carryover_message.id)
                    else:
                        carryover_message = await channel.fetch_message(carryover_message.id)

                    while True:
                        try:
                            carryover = await self.bot.wait_for("message", check=check, timeout=self.config["QUESTIONNAIRE"]["TIME"]["CARRYOVER"])  # Wait for a message

                            if ":" not in carryover.content:
                                minutes = int(int(carryover.content) / 60)
                                seconds = int(carryover.content) % 60

                                if len(str(seconds)) == 1:
                                    seconds = str(f"0{seconds}")

                                carryover.content = f"{minutes}:{seconds}"

                            break
                        except asyncio.TimeoutError:  # If they don't reply in the space of 60s terminate
                            if (self.config["QUESTIONNAIRE"]["MODE"] == "DM"):
                                carryover_updated = await reservation_user.dm_channel.fetch_message(carryover_message.id)
                            else:
                                carryover_updated = await channel.fetch_message(carryover_message.id)

                            if carryover_message.reactions[0].count != carryover_updated.reactions[0].count:
                                break

                message_config = self.config["QUESTIONNAIRE"]["CONFIRMATION_EMBED"]  # Gets the config for this message

                counter = 1

                # Creates and adds all the required fields to the embed
                embed = discord.Embed(
                    description=f"```{message_config['DESCRIPTION']}```",
                    colour=discord.colour.Colour.from_rgb(255, 128, 192))
                embed.set_thumbnail(url=self.bosses[self.boss_num]["icon"])
                embed.add_field(name=f"{counter}. {message_config['1. ID']}",
                                value=f"```{reservation_user.id}```",
                                inline=True)
                counter += 1
                embed.add_field(name=f"{counter}. {message_config['2. NAME']}",
                                value=f"```{reservation_user.display_name}```",
                                inline=True)
                counter += 1
                embed.add_field(name=f"{counter}. {message_config['3. BOSS']}",
                                value=f"```{self.round_num + 1}-{self.boss_num + 1}```",
                                inline=True)
                counter += 1
                embed.add_field(name=f"{counter}. {message_config['4. DAMAGE']}", value=f"```{damage.content}```")
                counter += 1
                embed.add_field(name=f"{counter}. {message_config['5. TYPE']}",
                                value=f"```{self.user_reaction_name.get(message.id)}```", inline=True)
                counter += 1
                embed.add_field(name=f"{counter}. {message_config['6. CARRYOVER_TIME']}",
                                value=f"```{carryover.content if 'carryover' in locals() else ' '}```", inline=True)
                
                if (self.config["QUESTIONNAIRE"]["MODE"] == "DM"):
                    confirm_message = await reservation_user.dm_channel.send(embed=embed)  # Sends the embed
                else:
                    confirm_message = await channel.send(embed=embed)  # Sends the embed

                for r in [utils.field_1_emoji, utils.field_2_emoji, utils.field_3_emoji, utils.field_4_emoji,
                          utils.field_5_emoji, utils.field_6_emoji, utils.send_emoji]:  # Adds all the needed reactions
                    await confirm_message.add_reaction(r)
                
                #New version with fixed timestamp
                to_update = {
                    "values": [
                        [datetime.now(timezone("Japan")).strftime("%Y/%m/%d %H:%M:%S"), f"{reservation_user.id}", None,
                         "'"+f"{self.round_num + 1}-{self.boss_num + 1}", damage.content, self.user_reaction_name.get(message.id),
                         "'"+str(carryover.content) if "carryover" in locals() else None]
                    ]
                }
                # A key for turning the reaction into a integer
                reaction_to_number = {utils.field_1_emoji: 1, utils.field_2_emoji: 2, utils.field_3_emoji: 3,
                                      utils.field_4_emoji: 4, utils.field_5_emoji: 5, utils.field_6_emoji: 6}

                while True:  # Starts a while loop until the questionnaire is completed or is terminated
                    # Gets a updated version of the embed
                    if (self.config["QUESTIONNAIRE"]["MODE"] == "DM"):
                        updated_message = await reservation_user.dm_channel.fetch_message(confirm_message.id)
                    else:
                        updated_message = await channel.fetch_message(confirm_message.id)

                    # A check to make sure reaction and user is correct
                    def check(r, u):
                        if (self.config["QUESTIONNAIRE"]["MODE"] == "DM"):
                            return str(r.emoji) in [utils.field_1_emoji, utils.field_2_emoji, utils.field_3_emoji,
                                                    utils.field_4_emoji, utils.field_5_emoji, utils.field_6_emoji,
                                                    utils.send_emoji] and u.id == \
                                   reservation_user.id and r.message.channel.id == reservation_user.dm_channel.id
                        else:
                            return str(r.emoji) in [utils.field_1_emoji, utils.field_2_emoji, utils.field_3_emoji,
                                                    utils.field_4_emoji, utils.field_5_emoji, utils.field_6_emoji,
                                                    utils.send_emoji] and r.message.channel.id == channel.id

                    try:
                        r, u = await self.bot.wait_for("reaction_add", check=check, timeout=30)  # Waits for a reaction
                    except asyncio.TimeoutError:  # If they do not react in 30 seconds
                        # Appends the row to spreadsheet with the information
                        utils.spreadsheet_handler.questionnaire_worksheet.append_row(to_update["values"][0], value_input_option='USER_ENTERED', insert_data_option="OVERWRITE",table_range="A1")
                        utils.spreadsheet_handler.questionnaire_col_count = utils.spreadsheet_handler.questionnaire_col_count +1
                        break  # breaks from the loop

                    if str(r.emoji) == utils.send_emoji:  # Check if the reaction is O
                        # Appends the new row into the spreadsheet with all the required information
                        utils.spreadsheet_handler.questionnaire_worksheet.append_row(to_update["values"][0], value_input_option='USER_ENTERED', insert_data_option="OVERWRITE",table_range="A1")
                        utils.spreadsheet_handler.questionnaire_col_count = utils.spreadsheet_handler.questionnaire_col_count +1
                        break  # breaks from the loop

                    elif str(r.emoji) in [utils.field_1_emoji, utils.field_2_emoji, utils.field_3_emoji,
                                          utils.field_4_emoji, utils.field_5_emoji,
                                          utils.field_6_emoji]:  # Checks if the reaction is a number
                        reaction_to_word = {utils.field_1_emoji: message_config['1. ID'],
                                            utils.field_2_emoji: message_config['2. NAME'],
                                            utils.field_3_emoji: message_config['3. BOSS'],
                                            utils.field_4_emoji: message_config['4. DAMAGE'],
                                            utils.field_5_emoji: message_config['5. TYPE'],
                                            utils.field_6_emoji: message_config['6. CARRYOVER_TIME']}
                                            
                        if (self.config["QUESTIONNAIRE"]["MODE"] == "DM"):
                            await reservation_user.dm_channel.send(
                                f"{reaction_to_word.get(r.emoji)}{self.config['QUESTIONNAIRE']['QUESTION_TEXT']['FIELD_EDIT']}")  # sends the required message
                        else:
                            await channel.send(
                                f"{reaction_to_word.get(r.emoji)}{self.config['QUESTIONNAIRE']['QUESTION_TEXT']['FIELD_EDIT']}")  # sends the required message

                        # a check to make sure the message is correct
                        def check(m):
                            if (self.config["QUESTIONNAIRE"]["MODE"] == "DM"):
                                return m.author.id == reservation_user.id and m.channel == \
                                       reservation_user.dm_channel
                            else:
                                return m.channel == channel

                        try:
                            edit = await self.bot.wait_for("message", check=check, timeout=60)  # waits for a messages
                        except asyncio.TimeoutError:  # if they don't reply in the space of 60s terminate
                            continue

                        # Changes the the item in the order
                        if (reaction_to_number.get(str(r.emoji)) == 6) or (reaction_to_number.get(str(r.emoji)) == 3):
                            to_update["values"][0][reaction_to_number.get(str(r.emoji))] = str("'")+edit.content
                        else:
                            to_update["values"][0][reaction_to_number.get(str(r.emoji))] = edit.content

                        # Changes the the item in the order to the new
                        confirm_embed_field_index = [utils.field_1_emoji, utils.field_2_emoji, utils.field_3_emoji,
                                                     utils.field_4_emoji,
                                                     utils.field_5_emoji, utils.field_6_emoji]

                        field_name = updated_message.embeds[0].fields[
                            confirm_embed_field_index.index(str(r.emoji))].name  # Gets the field name

                        updated_message.embeds[0].set_field_at(index=confirm_embed_field_index.index(str(r.emoji)),
                                                               name=field_name, value=f"```{edit.content}```",
                                                               inline=True)  # Sets the new value in the field

                        await updated_message.edit(embed=updated_message.embeds[0])  # Edits the field

        if str(payload.emoji) == utils.check_in_emoji and user.id != self.bot.user.id and message.id == \
                self.boss_reservation_msgs[self.boss_num].id:
            """
            1. changes the value on the command message
            2. Adds the tick reaction to all the users reservations
            """
            # A key for the index of emojis
            embed_field_index = [utils.physical_emoji, utils.magic_emoji, utils.carryover_emoji, utils.check_in_emoji]

            # Calculates the new value for the embed
            new_value = int(
                self.current_boss_msg.embeds[0].fields[embed_field_index.index(utils.check_in_emoji)].value) + 1
            # Gets the field name
            field_name = self.current_boss_msg.embeds[0].fields[embed_field_index.index(utils.check_in_emoji)].name

            # Changes the field in the embed
            self.current_boss_msg.embeds[0].set_field_at(index=embed_field_index.index(utils.check_in_emoji),
                                                         name=field_name,
                                                         value=str(new_value), inline=True)

            # Edits the message
            await self.current_boss_msg.edit(embed=self.current_boss_msg.embeds[0])

            for id in self.user_reservation_msgs.keys():  # Loops through all the reservation message's ids
                # Checks if the messages user is the same as user who reacted
                if self.user_reservation_msgs.get(id).id == user.id:
                    # Gets the command channel
                    command_channel = discord.utils.get(user.guild.text_channels,
                                                        id=self.config["COMMAND"]["CHANNEL_ID"])
                    message = await command_channel.fetch_message(id)  # Gets the message

                    await message.add_reaction(utils.check_in_emoji)  # Adds the tick reaction to the message

            rowCount = 1
            cells = []
            for row in utils.all_reservation_row_data:
                if row[0] == str(user.id):
                    #utils.spreadsheet_handler.reservation_worksheet.update_cell(rowCount, 4, "TRUE") #Will batch it at the end instead
                    utils.all_reservation_row_data[rowCount-1][3] = "TRUE"
                    cells.append(utils.googleSheetAPI.models.Cell(rowCount, 4, "TRUE"))
                rowCount = rowCount + 1
            
            if len(cells) > 0:
                if not self.config["RESERVATION"]["MODE"] == "OFF":
                    utils.spreadsheet_handler.reservation_worksheet.update_cells(cells)

    @commands.Cog.listener()
    async def on_raw_reaction_remove(self, payload):
        """
		using raw reaction to prevent message cache issues
        """
        
        try:
            guildObj = self.bot.get_guild(int(payload.guild_id))
            user = guildObj.get_member(int(payload.user_id))
        except:
            user = self.bot.get_user(int(payload.user_id))
        if user is None:
            user = self.bot.get_user(int(payload.user_id))
        
        #print(user)
        channel = self.bot.get_channel(int(payload.channel_id))
        messageObj = await channel.fetch_message(int(payload.message_id))
        emoji = payload.emoji
        
        # Checks if the emoji and user are correct
        if str(emoji) in [utils.physical_emoji, utils.magic_emoji, utils.carryover_emoji] and user.id != \
                self.bot.user.id and messageObj.id == self.boss_reservation_msgs[self.boss_num].id:
            # Sets a key to help the bot get the index of the emoji
            embed_field_index = [utils.physical_emoji, utils.magic_emoji, utils.carryover_emoji,utils.check_in_emoji]

            # Calculates the new value for the embed
            new_value = int(self.current_boss_msg.embeds[0].fields[embed_field_index.index(str(emoji))].value) - 1
            # Gets the field name
            field_name = self.current_boss_msg.embeds[0].fields[embed_field_index.index(str(emoji))].name

            # Changes the field
            self.current_boss_msg.embeds[0].set_field_at(index=embed_field_index.index(str(emoji)),
                                                         name=field_name, value=str(new_value), inline=True)

            for id in self.user_reservation_msgs:  # Loops through all the reservation messages
                if self.user_reservation_msgs.get(id) == user:  # Checks if it's the same user who reacted
                    command_channel = discord.utils.get(user.guild.text_channels,
                                                        id=self.config["COMMAND"]["CHANNEL_ID"])
                    message = await command_channel.fetch_message(id)
                    await message.delete()  # Deletes the message

                    self.user_reservation_msgs.pop(id)  # Removes it from the dictionary

                    break  # Breaks from the loop

            # Find the cell with the ID that matches
            #cell = utils.spreadsheet_handler.reservation_worksheet.find(str(utils.spreadsheet_handler.spreadsheet_user_id.get(f"{user.id}-{str(emoji)}"))) #Will use row cache instead
            #print(utils.spreadsheet_handler.spreadsheet_user_id)
            
            rowcount = 1
            #print("Row it should delete: "+str(utils.spreadsheet_handler.spreadsheet_user_id.get(f"{user.id}-{str(emoji)}")))
            for row in utils.all_reservation_row_data:
                if str(row[6]) == str(utils.spreadsheet_handler.spreadsheet_user_id.get(f"{user.id}-{str(emoji)}")):
                    # If the user has check-in then it doesn't delete the row
                    #if str(row[3]) != "TRUE": #Removed
                    if not self.config["RESERVATION"]["MODE"] == "OFF":
                        utils.spreadsheet_handler.reservation_worksheet.delete_row(rowcount)
                    utils.spreadsheet_handler.reservation_col_count = utils.spreadsheet_handler.reservation_col_count - 1
                    del utils.all_reservation_row_data[rowcount-1]
                    break
                rowcount = rowcount +1
            
            #Resync blue checkmarks to cahced data
            numberOfCheckmarks = 0
            usersSeen = []
            for row in utils.all_reservation_row_data:
                if not str(row[1]) in usersSeen:
                    if str(row[3]) == "TRUE":
                        numberOfCheckmarks = numberOfCheckmarks+1
                        usersSeen.append(str(row[1]))
            new_value = numberOfCheckmarks
            field_name = self.current_boss_msg.embeds[0].fields[embed_field_index.index(str(utils.check_in_emoji))].name
            self.current_boss_msg.embeds[0].set_field_at(index=embed_field_index.index(str(utils.check_in_emoji)),name=field_name, value=str(new_value), inline=True)
            await messageObj.remove_reaction(utils.check_in_emoji, user)
            # Makes the edit to the message
            await self.current_boss_msg.edit(embed=self.current_boss_msg.embeds[0])
                
    @commands.command(name="clear")
    async def clear(self,ctx, amount=5):
        if str(ctx.message.author.id) in self.config["CLEAR_HISTORY"]["CLIENT_ID"]:
            await ctx.channel.purge(limit=amount)

def setup(bot):
    bot.add_cog(ManageMatch(bot))