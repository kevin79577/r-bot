from discord.ext import commands
import discord
import os
from datetime import datetime


class listAllRoleMembers(commands.Cog):
	def __init__(self, bot):
		self.bot = bot

	@commands.command(name="listRole")
	async def role_command(self, ctx,role):
		if ctx.message.guild:
			
			try:
				role = ctx.message.guild.get_role(int(role))
			except:
				role = discord.utils.get(ctx.message.guild.roles, mention=role)
			if role is None:
				await ctx.send("Invalid role")
				return False
			messageToSend = "```py\nDiscord ID            Discord Nickname\n\n"
			for member in role.members:
				messageToSend = messageToSend +str(member.id)+"    "+str(member)+"\n"
			
			messageToSend = messageToSend + "\n```"
			await ctx.send(messageToSend)
			
def setup(bot):
	bot.add_cog(listAllRoleMembers(bot))