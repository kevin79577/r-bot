import json
from oauth2client.service_account import ServiceAccountCredentials
import googleSheetAPI
import discord


def read_json(path):
    """
    reads a json file and return it back as python dictionary
    :param path: a file path to read
    :return: dictionary
    """
    try:
        with open(path, "r", encoding="utf-8") as file:  # Opens json path
            json_dict = json.load(file, encoding="utf-8")  # Converts file to dict
    except json.JSONDecodeError:  # Catches errors
        return False

    return json_dict

def save_json(path,data):

    with open(path, 'w', encoding="utf-8") as file:
        json.dump(data, file)

config = read_json("configs/config.json")  # Reads the config
bosses = read_json("configs/bosses.json")  # Gets a dict of the bosses.json file
try:
	memberChannels = read_json("configs/memberChannels.json")  # Gets a dict of the memberChannels.json file
except:
	memberChannels = {}

# Gets all emojis
physical_emoji = str(config["RESERVATION"]["EMOJI"]["PHYSIC"])
magic_emoji = str(config["RESERVATION"]["EMOJI"]["MAGIC"])
carryover_emoji = str(config["RESERVATION"]["EMOJI"]["CARRYOVER"])
check_in_emoji = str(config["COMMAND"]["EMOJI"]["CHECK_IN"])
confirm_emoji = str(config["COMMAND"]["EMOJI"]["CONFIRM"])
last_attack_emoji = str(config["COMMAND"]["EMOJI"]["LAST_ATTACK"])
task_kill_emoji = str(config["TASK_KILL"]["EMOJI"]["TASK_KILL"])
discard_emoji = str(config["QUESTIONNAIRE"]["EMOJI"]["DISCARD"])
send_emoji = str(config["QUESTIONNAIRE"]["EMOJI"]["SEND"])
field_1_emoji = str(config["QUESTIONNAIRE"]["EMOJI"]["FIELD_1"])
field_2_emoji = str(config["QUESTIONNAIRE"]["EMOJI"]["FIELD_2"])
field_3_emoji = str(config["QUESTIONNAIRE"]["EMOJI"]["FIELD_3"])
field_4_emoji = str(config["QUESTIONNAIRE"]["EMOJI"]["FIELD_4"])
field_5_emoji = str(config["QUESTIONNAIRE"]["EMOJI"]["FIELD_5"])
field_6_emoji = str(config["QUESTIONNAIRE"]["EMOJI"]["FIELD_6"])

async def send_command_boss_msg(round_num, boss_num, command_channel):
    """
    sends the current boss information to the command channel
    :param ctx: context
    :param round_num: the current round number
    :param boss_num: the current boss number
    :return: message object of current boss in command channel
    """
    message_config = config["COMMAND"]["UI_TEXT_EMBED"]  # Gets all the required information

    embed = discord.Embed(title=f"{round_num + 1} {message_config['ROUND']} - {bosses[boss_num]['name']}",
                          colour=discord.colour.Colour.from_rgb(255, 128, 192))  # Creates the embed
    embed.set_thumbnail(url=bosses[boss_num]["icon"])  # Specifics a thumbnail

    # Adds all fields to embed
    embed.add_field(name=f"{physical_emoji} {message_config['PHYSIC']}", value="0", inline=True)
    embed.add_field(name=f"{magic_emoji} {message_config['MAGIC']}", value="0", inline=True)
    embed.add_field(name=f"{carryover_emoji} {message_config['CARRYOVER']}", value="0", inline=True)
    embed.add_field(name=f"{check_in_emoji} {message_config['CHECK_IN']}", value="0", inline=True)
    embed.add_field(name=f"{confirm_emoji} {message_config['CONFIRM']}", value="0", inline=True)
    embed.add_field(name=f"{last_attack_emoji} {message_config['LAST_ATTACK']}", value="-", inline=True)

    return await command_channel.send(embed=embed)  # Sends embed to channel and return the message


async def send_reservation_boss_msg(round_num, boss_num, reservation_channel):
    """
    sends bosses information for the current round to reservation channel and adds reaction to allow user to place reservations
    :param round_num:
    :param boss_num:
    :param reservation_channel:
    :return:
    """
    message_config = config["RESERVATION"]["UI_TEXT_TABLE"]  # Gets all required information

    await reservation_channel.send(f"{round_num + 1} {message_config['ROUND']} ")  # Sends the round information
    reaction_key_msg = await reservation_channel.send(
        f"```{message_config['REFERENCE']}```")  # Sends a reference guide for emojis

    reactions_to_add = [physical_emoji, magic_emoji, carryover_emoji,
                        check_in_emoji]  # List of all the emojis to be added to the message

    # Loops through and adds reactions to reference message
    for reaction in reactions_to_add:
        await reaction_key_msg.add_reaction(reaction)

    await reservation_channel.send(message_config["SEPARATER"])  # Sends a divider message

    boss_reservation_msgs = []

    # loops through bosses and checks what bosses have been completed in the previous saved round (/finstop) and then
    # sends a message with the information of the boss and adds the needed reaction then adds it to a list of messages
    for boss in bosses:
        if boss_num <= bosses.index(boss):
            boss_message = await reservation_channel.send(
                f"**{bosses.index(boss) + 1} {message_config['BOSS']} - {boss['name']}**")

            for reaction in reactions_to_add:
                await boss_message.add_reaction(reaction)

            boss_reservation_msgs.append(boss_message)
        else:
            boss_message = ""
            boss_reservation_msgs.append(boss_message)
    
    #print("Boss reservation msgs ",str(boss_reservation_msgs))
    return boss_reservation_msgs  # Returns list of boss messages


class SpreadsheetHandler:
    def __init__(self):
        spreadsheet_config = read_json("configs/spreadsheet_config.json")  # Reads the spreadsheet config

        scope = ["https://spreadsheets.google.com/feeds", "https://www.googleapis.com/auth/spreadsheets",
                 "https://www.googleapis.com/auth/drive.file",
                 "https://www.googleapis.com/auth/drive"]  # Defines the client scopes

        creds = ServiceAccountCredentials.from_json_keyfile_name("secret/credentials.json",scope)  # Creates a Service Account Creds
		
        client = googleSheetAPI.authorize(creds).open_by_key(spreadsheet_config["SPREADSHEET_KEY"])  # Authorises the client

        self.reservation_worksheet = client.worksheet(
            spreadsheet_config["LOG_SHEET_RESERVATION"])  # Gets the reservation worksheet
        self.questionnaire_worksheet = client.worksheet(
            spreadsheet_config["LOG_SHEET_QUESTIONNAIRE"])  # Gets the questionnaire worksheet

        self.reservation_col_count = 2
        #self.questionnaire_col_count = len(self.questionnaire_worksheet.col_values(1))+1
        self.questionnaire_col_count = 2
        #print(self.questionnaire_col_count)
        
        self.reservation_spreadsheet_ids = 0
        self.spreadsheet_user_id = {}
        self.all_reservation_row_data = []
        #self.all_reservation_row_data = self.reservation_worksheet.get_all_values() #remove this after testing not really needed
        #print(self.all_reservation_row_data)


spreadsheet_handler = SpreadsheetHandler()
